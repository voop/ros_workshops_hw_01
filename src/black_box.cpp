#include <ros/ros.h>
#include <std_msgs/String.h>

boost::shared_ptr<ros::Publisher> pub;

void callback(const std_msgs::StringConstPtr & msg) {

	std_msgs::String msg_out;
	if (msg->data == "ros") {
		msg_out.data = "PIVO";
	} else
		msg_out.data = "Wrong!";
	pub->publish(msg_out);
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "black_box");
	ros::NodeHandle nh("~");

	pub.reset(new ros::Publisher);
	*pub = nh.advertise<std_msgs::String>("/black_box/out", 0);
	ros::Subscriber sub = nh.subscribe("/black_box/in", 0, callback);

	ros::spin();

	return 0;
}
