#include <ros/ros.h>
#include <ros_workshops_hw_01/Simple.h>

int main(int argc, char **argv) {
	ros::init(argc, argv, "simple_client");
	ros::NodeHandle nh;

	ros_workshops_hw_01::Simple srv;
	srv.request.input_a.data = "Hello ";
	srv.request.input_b.data = "ros";
	ros::service::call("simple_service", srv);

	ROS_WARN_STREAM(srv.response.output.data);

	return 0;

}
