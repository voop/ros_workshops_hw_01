#include <ros/ros.h>
#include <ros_workshops_hw_01/Simple.h>

bool callback(ros_workshops_hw_01::SimpleRequest & req, ros_workshops_hw_01::SimpleResponse & res) {

	res.output.data = req.input_a.data + req.input_b.data;
	return true;
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "simple_service");
	ros::NodeHandle nh;

	ROS_INFO_STREAM("Node launched.");

	ros::ServiceServer server = nh.advertiseService("simple_service", callback);

	ros::spin();

	return 0;

}
